import { async, ComponentFixture, TestBed } from '@angular/core/testing';


import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {

  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have <h1> with " In racing there are always things you can learn, every single day. "', () => {
    const heroElement: HTMLElement = fixture.nativeElement;
    const heading = heroElement.querySelector('h1');
    expect(heading.textContent).toContain('In racing there are always things you can learn, every single day.');
  });

  it('should have <h2> with "There is always space for improvement, and I think that applies to everything in life."', () => {
    const heroElement: HTMLElement = fixture.nativeElement;
    const heading = heroElement.querySelector('h2');
    expect(heading.textContent).toContain('There is always space for improvement, and I think that applies to everything in life.');
  });


});
