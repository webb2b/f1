import { Injectable } from '@angular/core';

import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class WebService {

  constructor(private http: Http) { }

  private baseUrl = '//ergast.com/api/f1/';

  getRequest(urlConfig): Observable<any> {

    let url = `${this.baseUrl}${urlConfig.url}`;
    
    return this.http.get(url)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

}
