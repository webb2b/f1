import { Injectable } from '@angular/core';


import { WebService } from './web.service';
import { Observable } from 'rxjs/Rx';
import { forkJoin } from "rxjs/observable/forkJoin";

@Injectable()
export class ApiService {

  constructor(private webService: WebService) { }

getChampionsList(): Observable <any>{
  let urlConfig = {'url': ''};
  let requests=[];
  for(let i=2005;i < 2016;i++){
    urlConfig.url=`${i}/driverStandings.json?limit=1`;
    requests.push(this.webService.getRequest(urlConfig));
  }

 return forkJoin(requests);

}

  getSeasonDetails(year): Observable<any> {

    let urlConfig = {
      'url': `${year}/results.json?limit=900`
    };
    return  this.webService.getRequest(urlConfig);

  }

}
