import { Injectable } from '@angular/core';

import { Subject }    from 'rxjs/Subject';

@Injectable()
export class DataService {

  constructor() { }
  
  private championSource = new Subject<any>();
  championObservable$ = this.championSource.asObservable();

  onChampionChangeInList(data: any) {
    this.championSource.next(data);
  }

}
