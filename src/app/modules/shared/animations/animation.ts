import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';
  
  import { AnimationEntryMetadata } from "@angular/core";
  
  export const listAnimation: AnimationEntryMetadata = trigger('listAnimation', [
    transition('* => *', [

      query(':enter', style({ opacity: 0 }), {optional: true}),

      query(':enter', stagger('50ms', [
        animate('1s ease-in', keyframes([
          style({opacity: 0, transform: 'translateY(-25%)', offset: 0}),
          style({opacity: .5, transform: 'translateY(5px)',  offset: 0.3}),
          style({opacity: 1, transform: 'translateY(0)',     offset: 1.0}),
        ]))]), {optional: true})
        
    ])

  ])


