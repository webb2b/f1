import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpModule } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';

/** third party modules */
import { AlertModule, AlertService } from 'ngx-alerts';
import { AsyncLocalStorageModule } from 'angular-async-local-storage';

/** services */
import { WebService } from '@shared/services/webservices/web.service';
import { ApiService } from '@shared/services/webservices/api.service';
import { DataService } from '@shared/services/data/data.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AsyncLocalStorage } from 'angular-async-local-storage';

import { ListComponent} from './list.component';


describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;
  let apiService : ApiService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
      declarations:[ListComponent],
      imports:[HttpModule,AsyncLocalStorageModule,AlertModule.forRoot({maxMessages: 5, timeout: 5000})],
      providers:[WebService, ApiService,DataService,Ng4LoadingSpinnerService,AlertService,AsyncLocalStorage]
        
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    apiService=TestBed.get(ApiService);
    fixture.detectChanges();
    
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it("selectedChampion should not be null when loadSeason is called",()=>{
    let champion = {"year":"2005","driver":{"driverId":"alonso"}};
    component.loadSeason(champion);
    expect(component.selectedChampion).not.toBe(null)
  });

});
