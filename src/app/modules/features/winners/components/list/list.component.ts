import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

/** shared services */
import { ApiService } from '@shared/services/webservices/api.service';
import { DataService } from '@shared/services/data/data.service';

/** third party services */
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AlertService } from 'ngx-alerts';
import { AsyncLocalStorage } from 'angular-async-local-storage';

import { listAnimation } from "@shared/animations/animation";

@Component({
  selector: 'season-list',
  templateUrl: './list.component.html',
  animations: [
    listAnimation
  ]
})
export class ListComponent implements OnInit, OnDestroy {

  champions = [];
  races = [];
  selectedChampion = null;
  subscription: Subscription;


  constructor(
    private apiService: ApiService,
    private dataService: DataService,
    private spinnerService: Ng4LoadingSpinnerService,
    private alertService: AlertService,
    private storage: AsyncLocalStorage) { }

  ngOnInit() {

    // check if champions are available in localstorage , if not request from the server
    this.storage.getItem('champions').subscribe((data) => {
      if (data === null) {
        this.requestDataFromServer();
      } else {
        this.champions = data;
      }

    }, () => {
      // Not called
    });


  }
  requestDataFromServer() {

    // start loading animation
    this.spinnerService.show();

    // request for champion list
    this.subscription = this.apiService.getChampionsList().subscribe(
      (response) => {
        this.champions = response.map((item) => {
          let obj = item.MRData.StandingsTable.StandingsLists.shift();
          let driver = obj.DriverStandings.shift();
          return {
            year: obj.season,
            driver: driver.Driver
          }
        })

        // store in localstorage
        this.storage.setItem('champions', this.champions).subscribe(() => { }, () => { });

        // hide loading animation   
        this.spinnerService.hide();
      },
      (error) => {
        // show error alert and hide loading animation   
        this.alertService.warning('Sorry, Something Went Wrong: Please try again later');
        this.spinnerService.hide();
      }
    );
  }

  loadSeason(champion) {
    this.selectedChampion = champion;
    this.dataService.onChampionChangeInList(champion);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
