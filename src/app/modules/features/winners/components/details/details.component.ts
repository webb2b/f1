import { Component, OnInit, OnDestroy } from '@angular/core';

import { listAnimation } from "@shared/animations/animation";

/** shared services */
import { ApiService } from '@shared/services/webservices/api.service';
import { DataService } from '@shared/services/data/data.service';

/** third party services */
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AlertService } from 'ngx-alerts';
import { AsyncLocalStorage } from 'angular-async-local-storage';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  animations: [
    listAnimation
  ]
})
export class DetailsComponent implements OnInit, OnDestroy {

  races = null;
  champion = null;
  subscription: Subscription;
  request: any;

  constructor(
    private apiService: ApiService,
    private dataService: DataService,
    private spinnerService: Ng4LoadingSpinnerService,
    private alertService: AlertService,
    private storage: AsyncLocalStorage) { }

  ngOnInit() {

    // subscribe for champion change
    this.subscription = this.dataService.championObservable$.subscribe(
      (recievedObj) => {

        this.champion = recievedObj;

        // check if champions are available in localstorage , if not request from the server
        this.storage.getItem(this.champion.year).subscribe((data) => {
          if (data === null) {
            this.requestDataFromServer();
          } else {
            this.races = data;
          }

        }, () => {
          // Not called
        });

      });


  }

  requestDataFromServer() {

    // cancel existing request
    if (this.request)
      this.request.unsubscribe();

    // start loading animation
    this.spinnerService.show();

    // start request for year data   
    this.request = this.apiService.getSeasonDetails(this.champion.year).subscribe(
      (response) => {

        // filter data based on race status
        this.races = response.MRData.RaceTable.Races.filter((item) => {
          item.Results = item.Results.filter((race) => race.status === 'Finished')
          return item;
        });

        // hide loading animation   
        this.spinnerService.hide();

        // store data in localstorage
        this.storage.setItem(this.champion.year, this.races).subscribe(() => { }, () => { });


      },
      (error) => {

        // show error alert and hide loading animation   
        this.alertService.warning('Sorry, Something Went Wrong: Please try again later');
        this.spinnerService.hide();
      }
    );

  };

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
