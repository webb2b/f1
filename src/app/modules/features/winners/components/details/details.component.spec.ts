import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';

 
/** third party modules */
import { AlertModule, AlertService } from 'ngx-alerts';
import { AsyncLocalStorageModule } from 'angular-async-local-storage';

/** services */
import { WebService } from '@shared/services/webservices/web.service';
import { ApiService } from '@shared/services/webservices/api.service';
import { DataService } from '@shared/services/data/data.service';

import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AsyncLocalStorage } from 'angular-async-local-storage';

import { DetailsComponent } from './details.component';

describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;
  let apiService : ApiService;
  let dataService : DataService;
  let storage : AsyncLocalStorage;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[HttpModule,AsyncLocalStorageModule,AlertModule.forRoot({maxMessages: 5, timeout: 5000})],
      declarations:[DetailsComponent],
      providers:[WebService, ApiService,DataService,Ng4LoadingSpinnerService,AlertService,AsyncLocalStorage]
      
      
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;
    apiService=TestBed.get(ApiService);
    dataService=TestBed.get(DataService);
    storage=TestBed.get(AsyncLocalStorage);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('storage service should have been called', () => {
    let champion = {"year":"2005","driver":{"driverId":"alonso","permanentNumber":"14","code":"ALO","url":"http:\/\/en.wikipedia.org\/wiki\/Fernando_Alonso","givenName":"Fernando","familyName":"Alonso","dateOfBirth":"1981-07-29","nationality":"Spanish"}};

    let data={MRData:{RaceTable:{Races:[{Results:[{"status":"Finished"}]}]}}};

  

    spyOn(storage,"getItem").and.callFake(()=>{
          return Observable.from([data]);
    })
    component.ngOnInit();
    dataService.onChampionChangeInList(champion);

    expect(storage.getItem).toHaveBeenCalled();
    });

  it('champion should not be null', () => {
    let champion = {"year":"2005","driver":{"driverId":"alonso","permanentNumber":"14","code":"ALO","url":"http:\/\/en.wikipedia.org\/wiki\/Fernando_Alonso","givenName":"Fernando","familyName":"Alonso","dateOfBirth":"1981-07-29","nationality":"Spanish"}};

    let data={MRData:{RaceTable:{Races:[{Results:[{"status":"Finished"}]}]}}};

  

    spyOn(apiService,"getSeasonDetails").and.callFake(()=>{
         return Observable.from([data]);
    })
    component.ngOnInit();
    dataService.onChampionChangeInList(champion);
    expect(component.champion).not.toBe(null);
  });


});
