import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './components/list/list.component';
import { DetailsComponent } from './components/details/details.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports:[
    ListComponent,
    DetailsComponent
  ],
  declarations: [ListComponent, DetailsComponent]
})
export class WinnersModule { }
