import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/** custom modules */
import { SharedModule } from '@shared/shared.module';
import { WinnersModule } from '@features/winners/winners.module';

/** services */
import { WebService } from '@shared/services/webservices/web.service';
import { ApiService } from '@shared/services/webservices/api.service';
import { DataService } from '@shared/services/data/data.service';

/** third party modules */
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { AlertModule } from 'ngx-alerts';
import { AsyncLocalStorageModule } from 'angular-async-local-storage';

import { AppComponent } from './app.component';




@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    SharedModule,
    WinnersModule,
    AsyncLocalStorageModule,
    Ng4LoadingSpinnerModule.forRoot(),
    AlertModule.forRoot({maxMessages: 5, timeout: 5000})
    
  ],
  providers: [
    WebService,
    ApiService,
    DataService
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
